function PointInTriangleRatio (p, p1, p2, p3) {

    console.log('PointInTriangleRatio', p, p1, p2, p3)

    // y = a1 * x + b1
    var dx1 = p3[0] - p2[0]
    console.log('dx1', dx1)
    var a1 = (p3[1] - p2[1]) / dx1
    var b1 = p2[1] - a1 * p2[0]
    console.log('a1', a1)
    console.log('b1', b1)

    // y = a2 * x + b2
    var dx2 = p1[0] - p[0]
    console.log('dx2', dx2)
    var a2 = (p1[1] - p[1]) / dx2
    var b2 = p[1] - a2 * p[0]
    console.log('a2', a2)
    console.log('b2', b2)

    var x, y
    if (dx1 === 0) {
        if (dx2 === 0) return null
        x = p2[0]
        y = a2 * p2[0] + b2
    } else if (dx2 === 0) {
        x = p[0]
        y = a1 * p[0] + b1
    } else {
        if (a1 === a2) return null
        // a1 * x + b1 = a2 * x + b2
        // a1 * x - a2 * x = b2 - b1
        // x * (a1 - a2) = b2 - b1
        x = (b2 - b1) / (a1 - a2)
        y = a1 * x + b1
    }
    console.log('x', x)
    console.log('y', y)

    var pdx = x - p[0]
    var pdy = y - p[1]

    var p1dx = x - p1[0]
    var p1dy = y - p1[1]

    if (pdx * p1dx < 0 || pdy * p1dy < 0) return null

    var pd = Math.sqrt(pdx * pdx + pdy * pdy)
    console.log('pd', pd)

    var p1d = Math.sqrt(p1dx * p1dx + p1dy * p1dy)
    console.log('p1d', p1d)
    if (p1d === 0) return null

    console.log('result', pd / p1d)
    return pd / p1d

}

function PointInTriangle (p, a, b, c) {
    return [
        PointInTriangleRatio(p, a, b, c),
        PointInTriangleRatio(p, b, c, a),
        PointInTriangleRatio(p, c, a, b),
    ]
}

var assert = require('assert')

var tests = [{
/*
    p: [3, 2],
    a: [1, 2],
    b: [3, 5],
    c: [5, 1],
    r: [
        0.42857142857142855,
        0.14285714285714285,
        0.42857142857142855,
    ],
}, {
    p: [4, 4],
    a: [1, 1],
    b: [2, 5],
    c: [5, 4],
    r: [
        0.07692307692307705,
        0.23076923076923073,
        0.6923076923076923,
    ],
}, {
    a: [1, 1],
    b: [2, 5],
    c: [5, 4],
    p: [4, 4],
    r: [
        0.07692307692307705,
        0.23076923076923073,
        0.6923076923076923,
    ],
}, {
    a: [1, 2],
    b: [5, 5],
    c: [5, 1],
    p: [4, 3],
    r: [
        0.24999999999999994,
        0.4375,
        0.3125,
    ],
}, {
    a: [1, 1],
    b: [3, 1],
    c: [5, 1],
    p: [2, 1],
    r: [null, null, null],
}, {
    a: [1, 1],
    b: [3, 1],
    c: [5, 1],
    p: [1, 1],
    r: [null, null, null],
}, {
    a: [1, 1],
    b: [1, 3],
    c: [1, 5],
    p: [2, 2],
    r: [
        null,
        null,
        null,
    ],
}, {
    a: [1, 1],
    b: [2, 5],
    c: [4, 3],
    p: [3, 2],
    r: [
        0.39999999999999997,
        null,
        0.7000000000000001,
    ],
}, {
    a: [1, 1],
    b: [2, 5],
    c: [4, 3],
    p: [4, 3],
    r: [0, 0, 1],
}, {
    a: [1, 1],
    b: [2, 5],
    c: [4, 3],
    p: [1, 1],
    r: [1, 0, 0],
}, {
    a: [1, 1],
    b: [2, 5],
    c: [4, 3],
    p: [2, 5],
    r: [0, 1, 0],
}, {
    a: [1, 1],
    b: [2, 5],
    c: [4, 3],
    p: [1.5, 3],
    r: [0.5, 0.5, 0],
}, {
    a: [1, 1],
    b: [1, 5],
    c: [4, 3],
    p: [1, 3],
    r: [0.5, 0.5, 0],
}, {
    a: [1, 1],
    b: [3, 3],
    c: [5, 5],
    p: [4.5, 4.5],
    r: [null, null, null],
}, {
    a: [1, 1],
    b: [5, 6],
    c: [5, 2],
    p: [5, 5],
    r: [0, 0.75, 0.25],
}, {
    a: [1, 1],
    b: [1, 6],
    c: [5, 2],
    p: [5, 5],
    r: [null, 0.6, null],
}, {
*/
    a: [0, 0],
    b: [10, -10, 0],
    c: [10, 10],
    p: [9.5, -9.5],
    r: [null, null, null],
}]

tests.forEach(function (test) {
    assert.deepEqual(PointInTriangle(test.p, test.a, test.b, test.c), test.r)
})
