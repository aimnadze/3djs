function PointInTriangleRatio (p, p1, p2, p3) {

    // y = a1 * x + b1
    var dx1 = p3[0] - p2[0]
    var a1 = (p3[1] - p2[1]) / dx1
    var b1 = p2[1] - a1 * p2[0]

    // y = a2 * x + b2
    var dx2 = p1[0] - p[0]
    var a2 = (p1[1] - p[1]) / dx2
    var b2 = p[1] - a2 * p[0]

    var x, y
    if (dx1 === 0) {
        if (dx2 === 0) return null
        x = p2[0]
        y = a2 * p2[0] + b2
    } else if (dx2 === 0) {
        x = p[0]
        y = a1 * p[0] + b1
    } else {
        if (a1 === a2) return null
        // a1 * x + b1 = a2 * x + b2
        // a1 * x - a2 * x = b2 - b1
        // x * (a1 - a2) = b2 - b1
        x = (b2 - b1) / (a1 - a2)
        y = a1 * x + b1
    }

    var pdx = x - p[0]
    var pdy = y - p[1]

    var p1dx = x - p1[0]
    var p1dy = y - p1[1]

    if (pdx * p1dx < 0 || pdy * p1dy < 0) return null

    var pd = Math.sqrt(pdx * pdx + pdy * pdy)

    var p1d = Math.sqrt(p1dx * p1dx + p1dy * p1dy)
    if (p1d === 0) return null

    return pd / p1d

}
