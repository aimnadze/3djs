function PointInTriangle (p, a, b, c) {

    var r1 = PointInTriangleRatio(p, a, b, c)
    if (r1 === null) return null

    var r2 = PointInTriangleRatio(p, b, c, a)
    if (r2 === null) return null

    var r3 = PointInTriangleRatio(p, c, a, b)
    if (r3 === null) return null

    return [r1, r2, r3]

}
