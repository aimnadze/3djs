function Matrix_RotateX (matrix, angle) {

    var c = Math.cos(angle),
        s = Math.sin(angle)

    var m1 = matrix[1],
        m2 = matrix[2],
        m4 = matrix[4],
        m5 = matrix[5],
        m7 = matrix[7],
        m8 = matrix[8]

    matrix[1] = m1 * c - m2 * s
    matrix[2] = m1 * s + m2 * c

    matrix[4] = m4 * c - m5 * s
    matrix[5] = m4 * s + m5 * c

    matrix[7] = m7 * c - m8 * s
    matrix[8] = m7 * s + m8 * c

}
