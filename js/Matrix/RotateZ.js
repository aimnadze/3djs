function Matrix_RotateZ (matrix, angle) {

    var c = Math.cos(angle),
        s = Math.sin(angle)

    var m0 = matrix[0],
        m1 = matrix[1],
        m3 = matrix[3],
        m4 = matrix[4],
        m6 = matrix[6],
        m7 = matrix[7]

    matrix[0] = m0 * c - m1 * s
    matrix[1] = m0 * s + m1 * c

    matrix[3] = m3 * c - m4 * s
    matrix[4] = m3 * s + m4 * c

    matrix[6] = m6 * c - m7 * s
    matrix[7] = m6 * s + m7 * c

}
