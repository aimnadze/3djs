function Matrix_Scale (matrix, scale) {

    var s0 = scale[0],
        s1 = scale[1],
        s2 = scale[2]

    matrix[0] *= s0
    matrix[1] *= s1
    matrix[2] *= s2

    matrix[3] *= s0
    matrix[4] *= s1
    matrix[5] *= s2

    matrix[6] *= s0
    matrix[7] *= s1
    matrix[8] *= s2

}
