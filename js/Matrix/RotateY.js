function Matrix_RotateY (matrix, angle) {

    var c = Math.cos(angle),
        s = Math.sin(angle)

    var m0 = matrix[0],
        m2 = matrix[2],
        m3 = matrix[3],
        m5 = matrix[5],
        m6 = matrix[6],
        m8 = matrix[8]

    matrix[0] = m0 * c - m2 * s
    matrix[2] = m0 * s + m2 * c

    matrix[3] = m3 * c - m5 * s
    matrix[5] = m3 * s + m5 * c

    matrix[6] = m6 * c - m8 * s
    matrix[8] = m6 * s + m8 * c

}
