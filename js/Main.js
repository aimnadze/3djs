function Main () {

    function repaint () {

        var context = canvas.getContext('2d')
        context.translate(0.5, 0.5)
        context.translate(canvas.width * 0.5, canvas.height * 0.5)

        var time = Date.now()
        Matrix_Identity(viewport)
        Matrix_RotateZ(viewport, time * 0.001)
        Matrix_RotateY(viewport, time * 0.0006)
        Matrix_RotateX(viewport, time * 0.0002)
        Matrix_Scale(viewport, [15, 15, 15])

        var transformeds = triangles3.map(function (vertices3) {
            return vertices3.map(function (vertex, vertex_index) {

                var x = vertex[0],
                    y = vertex[1],
                    z = vertex[2]

                return [
                    x * viewport[0] + y * viewport[1] + z * viewport[2],
                    x * viewport[3] + y * viewport[4] + z * viewport[5],
                    x * viewport[6] + y * viewport[7] + z * viewport[8],
                ]

            })
        })

        var projecteds = []
        transformeds.forEach(function (vertices3) {
            var vertices2 = []
            vertices3.forEach(function (vertex) {

                var x = vertex[0],
                    y = vertex[1],
                    z = vertex[2]

                var px = x * perspective / (perspective - z),
                    py = y * perspective / (perspective - z)

                vertices2.push([px, py])

            })
            projecteds.push(vertices2)
        })

        for (var i = 0; i < depths.length; i++) depths[i] = -Infinity

        var imageData = context.createImageData(canvas.width, canvas.height)
        var offset = 0
        for (var y = 0; y < canvas.height; y++) {
            for (var x = 0; x < canvas.width; x++) {

                var color = background
                projecteds.forEach(function (vertices2, index) {

                    var p = [-canvas.width * 0.5 + 0.5 + x, -canvas.height * 0.5 +  0.5 + y]
                    var ratio = PointInTriangle(p, vertices2[0], vertices2[1], vertices2[2])
                    if (ratio === null) return

                    var vertices3 = transformeds[index]

                    var depth =
                        vertices3[0][2] * ratio[0] +
                        vertices3[1][2] * ratio[1] +
                        vertices3[2][2] * ratio[2]

                    var depth_index = y * canvas.width + x
                    if (depths[depth_index] > depth) return
                    depths[depth_index] = depth

                    color = colors[index]

                })

                imageData.data[offset] = color[0] * 255
                imageData.data[offset + 1] = color[1] * 255
                imageData.data[offset + 2] = color[2] * 255
                imageData.data[offset + 3] = 255

                offset += 4

            }
        }
        context.putImageData(imageData, 0, 0)

        var offset = 0
        var depthContext = depthCanvas.getContext('2d')
        var imageData = depthContext.createImageData(canvas.width, canvas.height)
        for (var i = 0; i < depths.length; i++) {

            var color = [0, 0, 0, 0]

            var ratio = (depths[i] - depth_min) / depth_scale
            if (ratio < 0) {
                color[0] = 0
                color[1] = 0
                color[2] = 0
                color[3] = 1
            } else if (ratio > 1) {
                color[0] = 1
                color[1] = 1
                color[2] = 1
                color[3] = 1
            } else {
                color[1] = ratio
                color[2] = 1 - ratio
                color[3] = 1
            }

            imageData.data[offset] = color[0] * 255
            imageData.data[offset + 1] = color[1] * 255
            imageData.data[offset + 2] = color[2] * 255
            imageData.data[offset + 3] = color[3] * 255

            offset += 4

        }
        depthContext.putImageData(imageData, 0, 0)

        requestAnimationFrame(repaint)

    }

    var background = [1, 1, 1]

    var perspective = 100

    var viewport = Matrix_New()

    var triangles3 = [
        [
            [-1, -1, 1],
            [-1, 1, 1],
            [1, -1, 1],
        ],
        [
            [-1, 1, 1],
            [1, -1, 1],
            [1, 1, 1],
        ],
        [
            [-1, 1, -1],
            [-1, 1, 1],
            [1, 1, -1],
        ],
        [
            [-1, 1, 1],
            [1, 1, -1],
            [1, 1, 1],
        ],
        [
            [1, -1, -1],
            [1, 1, -1],
            [1, -1, 1],
        ],
        [
            [1, 1, -1],
            [1, -1, 1],
            [1, 1, 1],
        ],
        [
            [-1, -1, -1],
            [-1, 1, -1],
            [1, -1, -1],
        ],
        [
            [-1, 1, -1],
            [1, -1, -1],
            [1, 1, -1],
        ],
        [
            [-1, -1, -1],
            [-1, -1, 1],
            [1, -1, -1],
        ],
        [
            [-1, -1, 1],
            [1, -1, -1],
            [1, -1, 1],
        ],
        [
            [-1, -1, -1],
            [-1, 1, -1],
            [-1, -1, 1],
        ],
        [
            [-1, 1, -1],
            [-1, -1, 1],
            [-1, 1, 1],
        ],
    ]

    var colors = [
        [1, 0, 0],
        [1, 0, 0],
        [0, 1, 0],
        [0, 1, 0],
        [0, 0, 1],
        [0, 0, 1],
        [1, 1, 0],
        [1, 1, 0],
        [0, 1, 1],
        [0, 1, 1],
        [1, 0, 1],
        [1, 0, 1],
    ]

    var multiplier = 5
    var size = 60

    var depth_max = 30,
        depth_min = -30,
        depth_scale = depth_max - depth_min

    var canvas = document.createElement('canvas')
    canvas.className = 'Main-canvas'
    canvas.style.width = canvas.style.height = size * multiplier + 'px'
    canvas.width = canvas.height = size
    canvas.style.borderWidth = multiplier + 'px'

    var depths = new Float32Array(canvas.width * canvas.height)

    var depthCanvas = document.createElement('canvas')
    depthCanvas.className = 'Main-canvas'
    depthCanvas.style.width = depthCanvas.style.height = size * multiplier + 'px'
    depthCanvas.width = depthCanvas.height = size
    depthCanvas.style.borderWidth = multiplier + 'px'

    var body = document.body
    body.appendChild(canvas)
    body.appendChild(depthCanvas)

    repaint()

}
